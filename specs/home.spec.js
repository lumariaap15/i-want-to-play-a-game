import Home from "../src/views/Home";
import { mount , createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex'
import VueRouter from 'vue-router'

const localVue = createLocalVue()
localVue.use(VueRouter)
localVue.use(Vuex)
const router = new VueRouter()

test('initial view renders correctly', ()=>{
    const wrapper = mount(Home)
    expect(wrapper).toMatchSnapshot()
})

test('user can provide his name',async ()=>{
    const wrapper = mount(Home)
    const name = "Luisa Alzate"

    const input = wrapper.find('input')

    input.element.value = name
    input.trigger('input')
    expect(wrapper.vm.username).toBe(name)

    expect(router.currentRoute.path).toBe('/')
})

test('user cannot login if no name is provided', async ()=>{
    const wrapper = mount(Home)
    const name = ""

    const input = wrapper.find('input')
    const button = wrapper.find('button')

    input.element.value = name
    input.trigger('input')
    expect(wrapper.vm.username).toBe(name)

    expect(router.currentRoute.path).toBe('/')

    button.trigger('click')

    wrapper.vm.$nextTick(()=>{
        expect(router.currentRoute.path).toBe('/')
    })
})

test('user is saved in vuex store',()=>{
    let store = new Vuex.Store({
        state:{
            user: null
        }
    })
    const wrapper = mount(Home, { store, localVue })
    expect(store.state.user).toBe(null)

    const name = "Luisa Alzate"

    const input = wrapper.find('input')
    const button = wrapper.find('button')

    input.element.value = name
    input.trigger('input')
    expect(wrapper.vm.username).toBe(name)
    button.trigger('click')

    wrapper.vm.$nextTick(()=>{
        expect(store.state.user).toBe(name)
    })
})

